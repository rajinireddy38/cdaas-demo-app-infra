# Ansible - AWS EC2 Instances

Demo Ansible Playbook for creating, stopping, terminating the EC2 instance

  - Create a custom Security Group in AWS to allow ports 22 (SSH), 80 (HTTP) and ICMP.
  - Create/Stop/Terminate the EC2 instance - Using Ansible EC2 module
  - Wait for the SSH is up on Instance.

## Installation

Firstly, you need to install and configure AWS CLI (if you don't already have it). Depending on your operating system and environment, there are many ways to install AWS CLI. You can read this article to setup AWS CLI [How to install and configure AWS CLI](http://devopsmates.com/install-configure-aws-cli-amazon-web-services-command-line-interface/).

#### 1. Create key_pair with name "AWS-Ansible"

```sh
ansible@ansible-node:~/ec2-instance$ aws ec2 create-key-pair --key-name "AWS-Ansible"
```

#### 2. Create Security Group in AWS to allow ports 22/SSH, 80/HTTP and ICMP.



```
---
- hosts: server
  tasks:
    - name: Setting up the Security Group for new instance
      ec2_group:
          name: Ansible_Security_Group_AWS
          description: Allowing Traffic on port 22 and 80
          region: ap-southeast-1
          rules:
           - proto: tcp
             from_port: 80
             to_port: 80
             cidr_ip: 0.0.0.0/0

           - proto: tcp
             from_port: 22
             to_port: 22
             cidr_ip: 0.0.0.0/0

           - proto: icmp
             from_port: -1
             to_port: -1
             cidr_ip: 0.0.0.0/0
          rules_egress:
           - proto: all
             cidr_ip: 0.0.0.0/0
          vpc_id: vpc-d8826ebd

    - name: Provision EC2 instance
      ec2:
         key_name: AWS-Ansible
         region: ap-southeast-1
         instance_type: t2.micro
         image: ami-67a6e604
         wait: yes
         wait_timeout: 500
         count: 1
         instance_tags:
            Name: AWS-Ansible
         volumes:
            - device_name: /dev/xvda
              volume_type: gp2
              volume_size: 8
         monitoring: yes
         vpc_subnet_id: subnet-18e7fa93
         assign_public_ip: yes
         group: Ansible_Security_Group_AWS
      register: ec2

    - name: Wait for SSH to come up
      wait_for:
          host: "{{ item.public_dns_name }}"
          port: 22
          delay: 60
          timeout: 320
          state: started
      with_items: "{{ ec2.instances }}"
```

#### 3. Execution Ansible playbooks to create EC2 instance

```sh
rareddy@lbnl734150:/mnt/c/Assignment/cdaas-infra-setup$ ansible-playbook -i hosts main/ec2-creation.yml 

PLAY [Server] *************************************************************************************************

TASK [Gathering Facts] ****************************************************************************************
ok: [127.0.0.1]

TASK [Create an EC2 key] **************************************************************************************
ok: [127.0.0.1]

TASK [Setting up the Security Group for the instances] ********************************************************
changed: [127.0.0.1]

TASK [Provision EC2 instance for Gitlab] **********************************************************************
changed: [127.0.0.1]

TASK [Wait for SSH to come up] ********************************************************************************
ok: [127.0.0.1] => (item={'id': 'i-0f75ec2a322434b44', 'ami_launch_index': '0', 'private_ip': '172.31.43.46', 'private_dns_name': 'ip-172-31-43-46.eu-west-1.compute.internal', 'public_ip': '3.250.12.226', 'dns_name': 'ec2-3-250-12-226.eu-west-1.compute.amazonaws.com', 'public_dns_name': 'ec2-3-250-12-226.eu-west-1.compute.amazonaws.com', 'state_code': 16, 'architecture': 'x86_64', 'image_id': 'ami-05e786af422f8082a', 'key_name': 'AWS-Gitlab-key', 'placement': 'eu-west-1a', 'region': 'eu-west-1', 'kernel': None, 'ramdisk': None, 'launch_time': '2023-01-06T03:53:24.000Z', 'instance_type': 't2.medium', 'root_device_type': 'ebs', 'root_device_name': '/dev/sda1', 'state': 'running', 'hypervisor': 'xen', 'tags': {'Name': 'AWS-Ansible-GitLab-server'}, 'groups': {'sg-0e5967f03653d7021': 'Ansible_Security_Group_AWS_Demo'}, 'virtualization_type': 'hvm', 'ebs_optimized': False, 'block_device_mapping': {'/dev/sda1': {'status': 'attached', 'volume_id': 'vol-0060ab048e1e9f807', 'delete_on_termination': True}, '/dev/xvda': {'status': 'attached', 'volume_id': 'vol-0f46c729b19b8e315', 'delete_on_termination': True}}, 'tenancy': 'default'})

TASK [Provision EC2 instance for Gitlab runner] ***************************************************************
changed: [127.0.0.1]

TASK [Wait for SSH to come up] ********************************************************************************
ok: [127.0.0.1] => (item={'id': 'i-02214bf9e81adb3d7', 'ami_launch_index': '0', 'private_ip': '172.31.32.6', 'private_dns_name': 'ip-172-31-32-6.eu-west-1.compute.internal', 'public_ip': '3.253.50.189', 'dns_name': 'ec2-3-253-50-189.eu-west-1.compute.amazonaws.com', 'public_dns_name': 'ec2-3-253-50-189.eu-west-1.compute.amazonaws.com', 'state_code': 16, 'architecture': 'x86_64', 'image_id': 'ami-05e786af422f8082a', 'key_name': 'AWS-Gitlab-key', 'placement': 'eu-west-1a', 'region': 'eu-west-1', 'kernel': None, 'ramdisk': None, 'launch_time': '2023-01-06T03:55:08.000Z', 'instance_type': 't2.medium', 'root_device_type': 'ebs', 'root_device_name': '/dev/sda1', 'state': 'running', 'hypervisor': 'xen', 'tags': {'Name': 'AWS-Ansible-GitLabRunner-server'}, 'groups': {'sg-0e5967f03653d7021': 'Ansible_Security_Group_AWS_Demo'}, 'virtualization_type': 'hvm', 'ebs_optimized': False, 'block_device_mapping': {'/dev/sda1': {'status': 'attached', 'volume_id': 'vol-0fa8386d0f7bc8dce', 'delete_on_termination': True}, '/dev/xvda': {'status': 'attached', 'volume_id': 'vol-0e14cd56e88d5d507', 'delete_on_termination': True}}, 'tenancy': 'default'})

PLAY RECAP ****************************************************************************************************127.0.0.1                  : ok=7    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

#### 4. Stop the EC2 instance

```
rareddy@lbnl734150:/mnt/c/Assignment/cdaas-infra-setup$ ansible-playbook -i hosts main/ec2-stop.yml -e gitlabid=i-0f75ec2a322434b44 -e gitlabrunnerid=i-02214bf9e81adb3d7

PLAY [Server] *************************************************************************************************

TASK [Stop GitLab EC2 instance] *******************************************************************************
changed: [127.0.0.1]

TASK [Stop GitLab Runner EC2 instance] ************************************************************************
changed: [127.0.0.1]

PLAY RECAP ****************************************************************************************************
127.0.0.1                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

#### 5. Terminate the EC2 instance

```
rareddy@lbnl734150:/mnt/c/Assignment/cdaas-infra-setup$ ansible-playbook -i hosts main/ec2-terminate.yml -e gitlabid=i-0f75ec2a322434b44 -e gitlabrunnerid=i-02214bf9e81adb3d7

PLAY [Server] *************************************************************************************************

TASK [Terminate GitLab EC2 instance] **************************************************************************
changed: [127.0.0.1]

TASK [Terminate GitLab Runner EC2 instance] *******************************************************************
changed: [127.0.0.1]

PLAY RECAP ****************************************************************************************************
127.0.0.1                  : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

